import React, { useEffect, useState } from 'react';
import InteractiveList from './list';
import FloatingActionButtonZoom from './floatingActionButton';
import ComposeEmail from './composeMailModal';
import ViewMail from './viewMailModal';
import {getAllMails, sendMail, draftMail} from './services/mails';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  const [isComposeModalOpen, setComposeModalToggle] = useState(false);
  const [isViewMailOpen, setViewMailToggle] = useState(false);
  const [viewMailData, setViewMailData] = useState({});

  const toggleCompose = (value) => {
    console.log('here ',value || isComposeModalOpen)
    setComposeModalToggle(value || !isComposeModalOpen)
  }

  const toggleViewMail = (value,data) => {
    console.log('here -- data',data)
    console.log('here -- val',value || isViewMailOpen)
    if (data) {
      setViewMailData(data)
    }
    setViewMailToggle(value || !isViewMailOpen)
  }

  const [mails, setMails] = useState([]);

  useEffect(() => {
    let mounted = true;
    getAllMails()
      .then(response => {
        if(mounted) {
          setMails(response.data.rows)
        }
      })
    return () => mounted = false;
  }, [])

  const sendEmail = async (e,data) => {
    try{
      console.log('sendMail',data)
      const response = await sendMail(data)
      toast.success(response.message)
    } catch (e) {
      toast.error(e)
    }
  }

  const addToDraft = async (e,data) => {
    try{
      console.log('addToDraft',data)
      const response = await draftMail(data)
      toast.success(response.message)
    } catch (e) {
      toast.error(e)
    }
  }

  return (
    <div className="App">
      <ToastContainer />
      <ComposeEmail isModalOpen={isComposeModalOpen} toggleModal={toggleCompose} onSendMail={sendEmail} onDraftMail={addToDraft}></ComposeEmail>
      <ViewMail isModalOpen={isViewMailOpen} toggleModal={toggleViewMail} mailData={viewMailData}></ViewMail>
      <InteractiveList viewMail={toggleViewMail} mails={mails} toast={toast}></InteractiveList>
      <FloatingActionButtonZoom openCompose={toggleCompose}></FloatingActionButtonZoom>
    </div>
  );
}

export default App;
