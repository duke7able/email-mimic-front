import CustomizedDialogs from './modal';

export default function ComposeEmail({isModalOpen, toggleModal,onSendMail,onDraftMail}) {

    return (
        <CustomizedDialogs isModalOpen={isModalOpen} toggleModal={toggleModal} title={'Compose Email'} isCompose={true} onSubmit={onSendMail} addToDraft={onDraftMail}></CustomizedDialogs>
    )
}