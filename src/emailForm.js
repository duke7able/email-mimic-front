import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

export default function EmailForm({isCompose,emailFormData,makeComposeData}) {
  const classes = useStyles();
  console.log('emailFormData',emailFormData)
  const [body, updateBody] = React.useState('multi');

  const changeBody = (e) => {
    updateBody(e.target.value)
    makeComposeData({body,receiver,subject})
  }

  const [receiver, setReceiver] = React.useState('shreyan@yopmail.com');

  const changeToAddress = (e) => {
    setReceiver(e.target.value)
    makeComposeData({body,receiver,subject})
  }

  const [subject, setSubject] = React.useState('Subject');

  const changeSubject = (e) => {
    setSubject(e.target.value)
    makeComposeData({body,receiver,subject})
  }

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <div>
        {isCompose ? (
          <>
          <TextField required id="standard-required" label="From" value={process.env.REACT_APP_MAIL_USERNAME} />
          <TextField required id="standard-required" label="To" value={receiver} onChange={changeToAddress}/>
          <TextField required id="standard-required" label="Subject" value={subject} onChange={changeSubject}/>
          </>
        ) : (
          <>
          <TextField disabled id="standard-disabled" label="From" defaultValue={emailFormData.senderEmail} />
          <TextField disabled id="standard-disabled" label="To" defaultValue={emailFormData.receiverEmail} />
          <TextField disabled id="standard-disabled" label="Subject" defaultValue={emailFormData.subject} />
          </>
        )}
      </div>
      <div>
        {isCompose?(
          <TextField
          id="outlined-multiline-flexible"
          label="Body"
          multiline
          rowsMax={4}
          defaultValue="Hello World"
          value={body}
          onChange={changeBody}
          variant="outlined"
        />
        ):(
          <TextField
          id="outlined-multiline-flexible"
          disabled
          label="Body"
          multiline
          rowsMax={4}
          defaultValue={emailFormData.body}
          variant="outlined"
        />
        )}
        {!isCompose && emailFormData.isDraft ? "Draft" : ""}
      </div>
    </form>
  );
}
