const getAllMails = () => {
    return fetch('http://localhost:8080/v1/listAllMail?offset=0&pageSize=0')
      .then(data => data.json())
}

const sendMail = (data) => {
  let formBody = [];
  for (let property in data) {
    const encodedKey = encodeURIComponent(property);
    const encodedValue = encodeURIComponent(data[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");
  return fetch('http://localhost:8080/v1/sendMail',{
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
    },
    body: formBody
  })
    .then(data => data.json())
}

const deleteMail = (id) => {
  const encodedKey = encodeURIComponent("deleteMailId");
  const encodedValue = encodeURIComponent(id);
  const formBody = encodedKey + "=" + encodedValue;
  
  return fetch('http://localhost:8080/v1/deleteMail',{
    method: 'delete',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
    },
    body: formBody
  })
    .then(data => data.json())
}

const draftMail = (data) => {
  let formBody = [];
  for (let property in data) {
    const encodedKey = encodeURIComponent(property);
    const encodedValue = encodeURIComponent(data[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");
  return fetch('http://localhost:8080/v1/draftMail',{
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
    },
    body: formBody
  })
    .then(data => data.json())
}

module.exports = {
  getAllMails,
  sendMail,
  deleteMail,
  draftMail
}