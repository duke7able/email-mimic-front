import CustomizedDialogs from './modal';

export default function ViewMail({isModalOpen, toggleModal,emailFormData,mailData}) {

    return (
        <CustomizedDialogs isModalOpen={isModalOpen} toggleModal={toggleModal} title={'View Email'} isCompose={false} emailFormData={mailData}></CustomizedDialogs>
    )
}