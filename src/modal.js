import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import EmailForm from './emailForm';

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function CustomizedDialogs({isModalOpen, toggleModal, title, isCompose, emailFormData, onSubmit, addToDraft}) {

  const [composeData, setComposeData] = React.useState({});

  const makeComposeData = (data) => {
    console.log('makeComposeData',data)
    setComposeData(data)
  }

  const handleClose = (e) => {
    if (isCompose) {
      console.log('handleClose -- save draft',composeData)
      addToDraft(e,composeData)
    }
    toggleModal(false);
  };

  const handleSubmit = (e,isCompose) => {
    if (isCompose) {
      console.log('handleSubmit',composeData)
      onSubmit(e,composeData)
    }
    console.log('handleSubmit','toggleModal');
    toggleModal(false);
  }

  return (
    <div>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={isModalOpen}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {title}
        </DialogTitle>
        <DialogContent dividers>
            <EmailForm isCompose={isCompose} emailFormData={emailFormData} makeComposeData={makeComposeData}></EmailForm>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={(e) => {handleSubmit(e,isCompose)}} color="primary">
            {isCompose?"Send Mail":"Ok"}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
