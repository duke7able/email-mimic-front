import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import DeleteIcon from '@material-ui/icons/Delete';
import AlertDialog from './agreeDisagreeModal';
import BookIcon from '@material-ui/icons/Book';
import SendIcon from '@material-ui/icons/Send';
import RepeatIcon from '@material-ui/icons/Repeat';
import {deleteMail, sendMail} from './services/mails';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
}));

function generate(mails,secondary,viewMail,toggleDeleteConfirmationModal,reSendEmail) {
    const list = !mails.length ? [] : mails
  return list.map((value) =>
    // React.cloneElement(element, {
    //   senderEmail: value.senderEmail,
    // }),
    {   
        const senderEmail = value.senderEmail
        return (
            <ListItem>
            <ListItemAvatar>
            <Avatar>
                {value.isDraft ? (
                    <BookIcon />
                ):(
                    <SendIcon />
                )}
            </Avatar>
            </ListItemAvatar>
            <ListItemText
            primary={value.receiverEmail + "  Subject: " + value.subject}
            secondary={secondary ? value.body : null}
            onClick={() => {viewMail(true,value)}}
            style={{cursor: 'pointer'}}
            />
            <ListItemSecondaryAction>
            <IconButton edge="end" aria-label="re-send" onClick={(e) => {reSendEmail(value)}}>
                <RepeatIcon />
            </IconButton>
            <IconButton edge="end" aria-label="delete" onClick={(e) => {toggleDeleteConfirmationModal(null,value.id)}}>
                <DeleteIcon />
            </IconButton>
            </ListItemSecondaryAction>
            </ListItem>
        )
    }
  );
}

export default function InteractiveList({viewMail, mails,toast}) {
  const classes = useStyles();
  const [dense, setDense] = React.useState(false);
  const [secondary, setSecondary] = React.useState(false);

  const [isDeleteConfirmationModalOpen, setDeleteConfirmModalToggle] = React.useState(false);
  const [currentDeleteSelected, setCurrentDeleteSelected] = React.useState(-1);

  const toggleDeleteConfirmationModal = (value,id) => {
    console.log('here',value || isDeleteConfirmationModalOpen)
    console.log('id',id)
    if (id) {
        setCurrentDeleteSelected(id)
    } else if (!value && isDeleteConfirmationModalOpen) {
        setCurrentDeleteSelected(-1)
    }
    setDeleteConfirmModalToggle(value || !isDeleteConfirmationModalOpen)
  }

  const deleteElement = async () => {
      try {
        console.log('deleteElement currentDeleteSelected',currentDeleteSelected)
        // call delete api
        const deleted = await deleteMail(currentDeleteSelected)
        toast.success(deleted.message)
      } catch (e) {
        toast.error(e.message)
      }
  }

  const reSendEmail = async (data) => {
      try {
        console.log('reSendEmail data',{
            body: data.body,
            receiver: data.receiverEmail,
            subject: data.subject
        })
        const reSend = await sendMail({
            body: data.body,
            receiver: data.receiverEmail,
            subject: data.subject
        })
        toast.success(reSend.message)
      } catch (e) {
        toast.error(e.message)
      }
    // call send mail with existing data
  }

  return (
    <div className={classes.root}>
        <AlertDialog isModalOpen={isDeleteConfirmationModalOpen} toggleModal={toggleDeleteConfirmationModal} onDelete={deleteElement}></AlertDialog>
      <FormGroup row>
        <FormControlLabel
          control={
            <Checkbox checked={dense} onChange={(event) => setDense(event.target.checked)} />
          }
          label="Enable dense"
        />
        <FormControlLabel
          control={
            <Checkbox
              checked={secondary}
              onChange={(event) => setSecondary(event.target.checked)}
            />
          }
          label="Enable Quick view Body"
        />
      </FormGroup>
      <Grid container >
        <Grid item xs={12} md={12} lg={12}>
          <div className={classes.demo}>
            <List dense={dense}>
              {/* {generate(
                <ListItem>
                  <ListItemAvatar>
                    <Avatar>
                      <FolderIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary="Single-line item"
                    secondary={secondary ? {senderEmail} : null}
                    onClick={viewMail}
                  />
                  <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="delete" onClick={setDeleteConfirmModalToggle}>
                      <DeleteIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              ,mails)} */}
              {generate(mails,secondary,viewMail,toggleDeleteConfirmationModal,reSendEmail)}
            </List>
          </div>
        </Grid>
      </Grid>
    </div>
  );
}
